// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const balok = require("./Function/Balok"); // import balok
const kerucut = require("./Function/kerucut"); // import kerucut
const limas = require("./Function/limasSegiEmpat"); // import limasSegiEmpat
const prisma = require("./Function/prisma"); // import prisma

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

// Function to display the menu
function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`1. Balok`);
  console.log(`2. Kerucut`);
  console.log(`3. Limas`);
  console.log(`4. Prisma`);
  console.log(`5. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      // If option is a number it will go here
      if (option == 1) {
        balok.inputLength(); // It will call input() function in cuboid file
      } else if (option == 2) {
        kerucut.inputLength(); // It will call input() function in cube file
      } else if (option == 3) {
        limas.inputPanjang(); // It will call inputPanjang() function in limasSegiEmpat file
      } else if (option == 4) {
        prisma.inputAlas();
      } else if (option == 5) {
        rl.close(); // It will close the program
      } else {
        console.log(`Option must be 1 to 5!\n`);
        menu(); // If option is not 1 to 5, it will go back to the menu again
      }
    } else {
      // If option is not a number it will go here
      console.log(`Option must be number!\n`);
      menu(); // If option is not 1 to 5, it will go back to the menu again
    }
  });
}

menu(); // call the menu function to display the menu

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
