// Import readline
const index = require("../index"); // Import index to run rl on this file

// Function to calculate Triangular Prism volume
function prismaS3(alas, height, tinggiP) {
  return ((alas * height)/2) * tinggiP;
}

// All input just in one code
function inputAlas() {
  index.rl.question("Alas: ", (alas) => {
    index.rl.question("Tinggi: ", (height) => {
      index.rl.question("Tinggi Prisma: ", (tinggiP) => {
        if (!isNaN(alas) && !isNaN(height)) {
          console.log(`\nTriangular Prism: ${prismaS3(alas, height, tinggiP)} \n`);
          index.rl.close();
        } else {
          console.log(`Alas, Tinggi, and Tinggi Prisma must be a number!\n`);
          inputAlas();
        }
      });
    });
  });
}

module.exports = { inputAlas }; // Export inputAlas, so another file can call it