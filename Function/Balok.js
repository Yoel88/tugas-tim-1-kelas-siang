const index = require("../index");

function balok(length, width, height) {
  return length * width * height;
}

function inputLength() {
  index.rl.question(`Length: `, (length) => {
    if (!isNaN(length)) {
      inputWidth(length);
    } else {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}

function inputWidth(length) {
  index.rl.question(`Width: `, (width) => {
    if (!isNaN(width)) {
      inputHeight(length, width);
    } else {
      console.log(`Width must be a number\n`);
      inputWidth(length);
    }
  });
}

function inputHeight(length, width) {
  index.rl.question(`Height: `, (height) => {
    if (!isNaN(height)) {
      console.log(`\nBalok: ${balok(length, width, height)}\n`);
      index.rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(length, width);
    }
  });
}

module.exports = { inputLength };
